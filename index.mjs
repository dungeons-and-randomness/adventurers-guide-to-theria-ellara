Hooks.once("init", () => {
  // Adds AGTE as a suggested book when you define the source of an item
  CONFIG.DND5E.sourceBooks.AGTE = "Adventurer's Guide to Theria - Ellara"

  // Adds endurant force powers as a class feature type
  CONFIG.DND5E.featureTypes.class.subtypes.fervorPower = "Fervor Power"
});