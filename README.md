# Adventurer's Guide to Theria - Ellara

Content from The Adventurer's Guide to Theria Volume 1 adapted for use with FoundryVTT.

## Included Content

- 34 new creatures
- 3 class augments
- 1 new class
- 2 new playable races
- Tons of rollable tables

## TODO

- [ ] Automate Endurant charge ability (will likely require the dnd5e.preUseItem hook)
- [ ] Steer into the Skid background
- [ ] Ten Thousand Horns
- [ ] White Wolf Valley
- [ ] The Northlands
- [ ] The Divide
- [ ] The Blackrock Mountains
- [ ] Heroes' March
- [ ] Dragons' Reach
- [ ] Hunters' Bounty
- [ ] The Hazaan Mountains
- [ ] The Wastes
- [ ] The Battered Coast
- [ ] The Islands
- [ ] Diseases
- [ ] Drugs
- [ ] Weather
- [ ] Endurants
- [ ] Revenants
- [ ] Werewolves
- [ ] Assorted Tables
- [ ] Bestiary
- [ ] Add creature links to Table of Wonder
- [ ] Add creature and item links to existing travel/encounter tables