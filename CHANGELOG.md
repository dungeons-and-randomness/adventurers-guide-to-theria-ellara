# Changelog

## 0.0.4

### New Compendium Items

- Resurrection tables

## 0.0.3 - 2024-10-1

### New Compendium Items

- Lizardfolk playable race
- Vanali playable race
- Crit tables
- Gate mechanics tables
- Rod and Table of Wonder

### Fixes

- Added action to Vampire regeneration to roll and apply healing

## 0.0.2 - 2024-6-26

### New Compendium Items

- Travel and encounter tables for The Mourning Shore
- Remaining backgrounds except for Steer into the Skid
- Herb items
- Vampire Augment


## 0.0.1 - 2024-6-24

This release is primarily intended to test the CI/CD pipeline; it should not be used in production environments.

### New Compendium Items

- Creature of the Night background